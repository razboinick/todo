Hello!

During testing this Todoes API I found two issues:

1. Something wrong with Integer.MIN_VALUE

**Steps**: 
 - run TodoPostTest.createTodoMinId test
 
**Expected result**: 201 status code 

**Actual result**: 400 status code

2. Sending PUT request with different ID in query and body leads to all todoes list changes

**Steps**: 
 - run TodoPutTest.updateTodoWithWrongIds test
 
**Expected result**: 422 status code 

**Actual result**: 200 status code
**Notes** Todo with id from request body will be created (but must not, because it is a PUT request).
