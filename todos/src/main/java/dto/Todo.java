package dto;

public class Todo {
    public Todo(){
    }

    public Todo ( int id, String text, boolean completed){
        this.id = id;
        this.text = text;
        this.completed = completed;
    }

    private int id;
    private String text;
    private boolean completed;

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public boolean getCompleted() {
        return completed;
    }

    public boolean equals(Object obj){
        Todo todo = (Todo) obj;
        if(this.id == todo.getId()
                && this.text.equals(todo.getText())
                && this.completed == todo.getCompleted()){
            return true;
        }
        return false;
    }
}
