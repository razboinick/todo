import com.fasterxml.jackson.core.JsonProcessingException;
import dto.Todo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Random;

import static util.TodoUtil.createTodoThroughApiForPerfomance;
import static util.TodoUtil.deleteTodoThroughApi;
import static util.TodoUtil.getTodosThroughApi;

public class PerformancePostTest {

    @AfterEach
    public void deleteTodos() throws JsonProcessingException {
        List<Todo> todos = getTodosThroughApi();
        for (Todo todo : todos) {
            deleteTodoThroughApi(todo.getId());
        }
    }

    @DisplayName("Performance test")
    public void performanceLikeTest() throws InterruptedException {
        int limit = 10;
        int portions = 5;

        Random random = new Random();
        int STEP = 100;

        for (int i = 0; i < portions; i++) {

//            System.out.println("creating " + i + "portion");

            for (int j = 0; j < limit; j++) {
                try {
                    Todo todo = new Todo(STEP + j, "auto test todo " + STEP + j, random.nextBoolean());
                    int statusCode = createTodoThroughApiForPerfomance(todo).statusCode();
                    System.out.println(i+","+todo.getId()+","+statusCode);
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }
            Thread.sleep(1000);
            STEP += 100;
        }
    }
}
