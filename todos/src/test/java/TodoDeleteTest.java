import com.fasterxml.jackson.core.JsonProcessingException;
import dto.Todo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static util.TodoUtil.changeTodoThroughApi;
import static util.TodoUtil.createTodoThroughApi;
import static util.TodoUtil.deleteTodoThroughApi;
import static util.TodoUtil.getTodosThroughApi;

@DisplayName("Tests for /delete endpoint")
public class TodoDeleteTest {

    List <Todo> todos = new ArrayList();

    @BeforeEach
    public void initTodos() throws JsonProcessingException {
        //first five let us be completed
        for(int i = 0; i < 5; i++){
            todos.add( new Todo(i, "auto test todo " + i, true));
        }

        //first five let us be non completed
        for(int i = 5; i < 10; i++){
            todos.add( new Todo(i, "auto test todo " + i, false));
        }

        for (Todo todo: todos) {
            createTodoThroughApi(todo);
        }
    }

    @AfterEach
    public void deleteTodos() throws JsonProcessingException {
        List<Todo> todos = getTodosThroughApi();
        for (Todo todo: todos) {
            deleteTodoThroughApi(todo.getId());
        }
    }

    @Test
    @DisplayName("Delete completed todo check")
    public void deleteCompletedTodo() throws JsonProcessingException {
        int completedId = 3;
        deleteTodoThroughApi(completedId);

        List<Todo> afterDeletion = getTodosThroughApi();

        assertFalse(afterDeletion.stream().anyMatch(item -> completedId == (item.getId())),
                "Todo with id "+completedId+" did not removed properly");

    }

    @Test
    @DisplayName("Delete non completed todo check")
    public void deleteNonCompletedTodo() throws JsonProcessingException {
        int completedId = 8;
        deleteTodoThroughApi(completedId);

        List<Todo> afterDeletion = getTodosThroughApi();

        assertFalse(afterDeletion.stream().anyMatch(item -> completedId == (item.getId())),
                "Todo with id "+completedId+" did not removed properly");

    }

    @Test
    @DisplayName("Delete all todoes check")
    public void deleteAllTodoes() throws JsonProcessingException {
        List<Todo> todos = getTodosThroughApi();
        for (Todo todo: todos) {
            deleteTodoThroughApi(todo.getId());
        }

        List<Todo> afterDeletion = getTodosThroughApi();
        assertTrue(afterDeletion.isEmpty(), "All todoes were not removed properly");
    }

    @Test
    @DisplayName("Delete changed todo check")
    public void deleteChangedTodo() throws JsonProcessingException {
        Todo todo = todos.get(4);
        todo.setText("autotest changed todo");
        todo.setCompleted(false);

        changeTodoThroughApi(todo);

        deleteTodoThroughApi(todo.getId());

        List<Todo> afterDeletion = getTodosThroughApi();

        assertFalse(afterDeletion.stream().anyMatch(item -> todo.getId() == (item.getId())),
                "Todo with id "+todo.getId()+" did not removed properly");
    }

    @Test
    @DisplayName("Check that all todoes are not changing after deletion")
    public void deleteTodoNotChangingOthers() throws JsonProcessingException {
        int id = 3;
        deleteTodoThroughApi(id);

        List<Todo> afterDeletion = getTodosThroughApi();
        List<Todo> expected = new ArrayList<>(todos);
        expected.remove(id);

        assertTrue(expected.size() == afterDeletion.size() && expected.containsAll(afterDeletion)
                &&afterDeletion.containsAll(expected),
                "Something went wrong after deletion and items have been changed");
        assertFalse(afterDeletion.stream().anyMatch(item -> id == (item.getId())),
                "Todo with id "+id+" did not removed properly");
    }

}
