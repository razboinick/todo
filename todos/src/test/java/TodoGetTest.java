import com.fasterxml.jackson.core.JsonProcessingException;
import dto.Todo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static util.TodoUtil.createTodoThroughApi;
import static util.TodoUtil.deleteTodoThroughApi;
import static util.TodoUtil.getTodosThroughApi;
import static util.TodoUtil.getTodosWithLimitThroughApi;
import static util.TodoUtil.getTodosWithOffsetAndLimitThroughApi;
import static util.TodoUtil.getTodosWithOffsetThroughApi;

public class TodoGetTest {
    List<Todo> todos = new ArrayList();

    @BeforeEach
    public void initTodos() throws JsonProcessingException {
        //first five let us be completed
        for (int i = 0; i < 5; i++) {
            todos.add(new Todo(i, "auto test todo " + i, true));
        }

        //first five let us be non completed
        for (int i = 5; i < 10; i++) {
            todos.add(new Todo(i, "auto test todo " + i, false));
        }

        for (Todo todo : todos) {
            createTodoThroughApi(todo);
        }
    }

    @AfterEach
    public void deleteTodos() throws JsonProcessingException {
        List<Todo> todos = getTodosThroughApi();
        for (Todo todo : todos) {
            deleteTodoThroughApi(todo.getId());
        }
    }

    @Test
    @DisplayName("Check get todos with offset less than todoes list's size")
    public void getTodoesWithNormalOffset() throws JsonProcessingException {
        int offset = 3;

        List<Todo> after = getTodosWithOffsetThroughApi(offset);
        List<Todo> expected = new ArrayList<>(todos);
        expected.subList(0, offset).clear();
        assertTrue(expected.size() == after.size() && expected.containsAll(after)
                        && after.containsAll(expected),
                "Something went wrong with get with offset = " + offset);
    }

    @Test
    @DisplayName("Check get todos with offset more than todoes list's size")
    public void getTodoesWithBigOffset() throws JsonProcessingException {
        int offset = todos.size()+10;

        List<Todo> after = getTodosWithOffsetThroughApi(offset);
        assertEquals(0, after.size(), "Something went wrong with get with offset = " + offset);
    }

    @Test
    @DisplayName("Check get todos with zero offset")
    public void getTodoesWithZeroOffset() throws JsonProcessingException {
        int offset = 0;

        List<Todo> after = getTodosWithOffsetThroughApi(offset);
        List<Todo> expected = new ArrayList<>(todos);
        assertTrue(expected.size() == after.size() && expected.containsAll(after)
                        && after.containsAll(expected),
                "Something went wrong with get with offset = " + offset);
    }


    @Test
    @DisplayName("Check get todos with limit less than todoes list's size")
    public void getTodoesWithNormalLimit() throws JsonProcessingException {
        int limit = 3;

        List<Todo> after = getTodosWithLimitThroughApi(limit);
        List<Todo> expected = new ArrayList<>(todos.subList(0, limit));
        assertTrue(expected.size() == after.size() && expected.containsAll(after)
                        && after.containsAll(expected),
                "Something went wrong with get with limit = " + limit);
    }

    @Test
    @DisplayName("Check get todos with limit more than todoes list's size")
    public void getTodoesWithBigLimit() throws JsonProcessingException {
        int limit = todos.size()+10;

        List<Todo> after = getTodosWithLimitThroughApi(limit);
        List<Todo> expected = new ArrayList<>(todos);
        assertTrue(expected.size() == after.size() && expected.containsAll(after)
                        && after.containsAll(expected),
                "Something went wrong with get with limit = " + limit);
    }

    @Test
    @DisplayName("Check get todos with zero limit")
    public void getTodoesWithZeroLimit() throws JsonProcessingException {
        int limit = 0;

        List<Todo> after = getTodosWithLimitThroughApi(limit);
        assertEquals(0, after.size(), "Something went wrong with get with limit = " + limit);
    }

    @Test
    @DisplayName("Get todoes with normal offset and limit")
    public void getTodoesWithLimitAndOffset() throws JsonProcessingException {
        int limit = 5;
        int offset = 2;

        List<Todo> after = getTodosWithOffsetAndLimitThroughApi(offset,limit);
        List<Todo> tempExpected = new ArrayList<>(todos);
        tempExpected.subList(0, offset).clear();
        List<Todo> expected = new ArrayList<>(tempExpected.subList(0, limit));

        assertTrue(expected.size() == after.size() && expected.containsAll(after)
                        && after.containsAll(expected),
                "Something went wrong with get with limit = " + limit +
                " and offset = " + offset);
    }
}
