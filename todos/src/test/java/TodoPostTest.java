import com.fasterxml.jackson.core.JsonProcessingException;
import dto.Todo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static util.TodoUtil.createExistingTodoThroughApi;
import static util.TodoUtil.createTodoThroughApi;
import static util.TodoUtil.deleteTodoThroughApi;
import static util.TodoUtil.getTodosThroughApi;

@DisplayName("Tests for /delete endpoint")
public class TodoPostTest {

    @AfterEach
    public void deleteTodos() throws JsonProcessingException {
        List<Todo> todos = getTodosThroughApi();
        for (Todo todo : todos) {
            deleteTodoThroughApi(todo.getId());
        }
    }

    @Test
    @DisplayName("Create completed todo")
    public void createCompletedTodo() throws JsonProcessingException {
        Todo todo = new Todo(1, "autotest completed todo", true);

        createTodoThroughApi(todo);

        List<Todo> todos = getTodosThroughApi();

        assertEquals(1, todos.size(), "Created more than one todo");
        assertTrue(todos.contains(todo), "Created todo is not found in result list");
    }

    @Test
    @DisplayName("Create non completed todo")
    public void createNonCompletedTodo() throws JsonProcessingException {
        Todo todo = new Todo(2, "autotest non completed todo", false);

        createTodoThroughApi(todo);

        List<Todo> todos = getTodosThroughApi();

        assertEquals(1, todos.size(), "Created more than one todo");
        assertTrue(todos.contains(todo), "Created todo is not found in result list");
    }

    @Test
    @DisplayName("Create todo after deleting the same")
    public void createDeletedTodo() throws JsonProcessingException {
        Todo todo = new Todo(3, "autotest deleted todo", true);

        createTodoThroughApi(todo);

        deleteTodoThroughApi(todo.getId());

        createTodoThroughApi(todo);

        List<Todo> todos = getTodosThroughApi();

        assertEquals(1, todos.size(), "Created more than one todo");
        assertTrue(todos.contains(todo), "Created todo is not found in result list");
    }

    @Test
    @DisplayName("Create existing todo")
    public void createExistingTodo() throws JsonProcessingException {
        Todo todo = new Todo(4, "autotest existing todo", true);

        createTodoThroughApi(todo);

        createExistingTodoThroughApi(todo);

        List<Todo> todos = getTodosThroughApi();

        assertEquals(1, todos.size(), "Created more than one todo");
        assertTrue(todos.contains(todo), "The first todo is not found in result list");
    }


    @Test
    @DisplayName("Create todo with max ID")
    public void createTodoMaxId() throws JsonProcessingException {
        Todo todo = new Todo(Integer.MAX_VALUE, "autotest completed todo", true);

        createTodoThroughApi(todo);

        List<Todo> todos = getTodosThroughApi();

        assertEquals(1, todos.size(), "Created more than one todo");
        assertTrue(todos.contains(todo), "Created todo is not found in result list");
    }

    @Test
    @DisplayName("Create todo with min ID")
    public void createTodoMinId() throws JsonProcessingException {
        Todo todo = new Todo(Integer.MIN_VALUE, "autotest completed todo", true);

        createTodoThroughApi(todo);

        List<Todo> todos = getTodosThroughApi();

        assertEquals(1, todos.size(), "Created more than one todo");
        assertTrue(todos.contains(todo), "Created todo is not found in result list");
    }
}
