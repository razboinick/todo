import com.fasterxml.jackson.core.JsonProcessingException;
import dto.Todo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static util.TodoUtil.changeNonExistingTodoThroughApi;
import static util.TodoUtil.changeTodoDifferentIdThroughApi;
import static util.TodoUtil.changeTodoThroughApi;
import static util.TodoUtil.createTodoThroughApi;
import static util.TodoUtil.deleteTodoThroughApiNotChecked;
import static util.TodoUtil.getTodosThroughApi;

public class TodoPutTest {

    List<Todo> todos = new ArrayList();

    @BeforeEach
    public void initTodos() throws JsonProcessingException {
        //first five let us be completed
        for(int i = 0; i < 5; i++){
            todos.add( new Todo(i, "autotest todo for put checks" + i, true));
        }

        //first five let us be non completed
        for(int i = 5; i < 10; i++){
            todos.add( new Todo(i, "autotest todo todo for put checks" + i, false));
        }

        for (Todo todo: todos) {
            System.out.println("created" +todo.getId());
            createTodoThroughApi(todo);
        }
    }

    @AfterEach
    public void deleteTodos() throws JsonProcessingException {
        List<Todo> todosToDelete = getTodosThroughApi();
        for (Todo todo: todosToDelete) {
            int statusCode = deleteTodoThroughApiNotChecked(todo.getId()).statusCode();
            System.out.println("deleted todo with id: "+todo.getId()+"with status code:"+ statusCode);

        }
    }

    @Test
    @DisplayName("Update completed todo check")
    public void updateCompletedTodo() throws JsonProcessingException {
        int id = 0;

        Todo newTodo = new Todo(id, "changed completed autotest todo", false);
        changeTodoThroughApi(newTodo);

        List<Todo> afterChanging = getTodosThroughApi();
        List<Todo> expected = new ArrayList<>(todos);
        expected.remove(id);
        expected.add(newTodo);

        assertTrue(expected.size() == afterChanging.size() && expected.containsAll(afterChanging)
                        &&afterChanging.containsAll(expected),
                "Something went wrong after updating todo with id "+id+" and items have been changed");
    }

    @Test
    @DisplayName("Update non completed todo check")
    public void updateNonCompletedTodo() throws JsonProcessingException {
        int id = 6;

        Todo newTodo = new Todo(id, "changed non completed autotest todo", false);
        changeTodoThroughApi(newTodo);

        List<Todo> afterChanging = getTodosThroughApi();
        List<Todo> expected = new ArrayList<>(todos);
        expected.remove(id);
        expected.add(newTodo);

        assertTrue(expected.size() == afterChanging.size() && expected.containsAll(afterChanging)
                        &&afterChanging.containsAll(expected),
                "Something went wrong after updating todo with id "+id+" and items have been changed");
    }

    @Test
    @DisplayName("Update non existing todo check")
    public void updateNonExistingTodo() throws JsonProcessingException {
        int id = 100;

        Todo newTodo = new Todo(id, "changed non existing autotest todo", false);
        changeNonExistingTodoThroughApi(newTodo);

        List<Todo> afterChanging = getTodosThroughApi();

        assertTrue(todos.size() == afterChanging.size() && todos.containsAll(afterChanging)
                        &&afterChanging.containsAll(todos),
                "Something went wrong after updating non existing todo with id "+id+" and items have been changed");
        assertFalse(afterChanging.stream().anyMatch(item -> id == (item.getId())),
                "Todo with id "+id+" should not exist in updated list of todoes");
    }

    @Test
    @DisplayName("Update todo with wrong request with contains different id check")
    public void updateTodoWithWrongIds() throws JsonProcessingException {
        int id = 2;

        Todo newTodo = new Todo(3, "changed todo", false);
        //here is a bug - I expect the 422 (Unprocessable Entity) status code
        //and test will fail here
        changeTodoDifferentIdThroughApi(newTodo,id);

        List<Todo> afterChanging = getTodosThroughApi();
        List<Todo> expected = new ArrayList<>(todos);

        //test must have this check that nothing is changed
        assertTrue(expected.size() == afterChanging.size() && expected.containsAll(afterChanging)
                        &&afterChanging.containsAll(expected),
                "Something went wrong after updating todo with id "+id+" and items have been changed");
    }
}
