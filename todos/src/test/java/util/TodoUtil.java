package util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dto.Todo;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.util.List;

import static io.restassured.RestAssured.given;

public class TodoUtil {

    public static void createTodoThroughApi(Todo todo) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String todoJson = objectMapper.writeValueAsString(todo);

        given().contentType(ContentType.JSON)
                .body(todoJson)
                .when()
                .auth()
                .basic("admin", "admin")
                .post("http://localhost:8080/todos")
                .then()
                .statusCode(201);
    }

    public static Response createTodoThroughApiForPerfomance(Todo todo) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String todoJson = objectMapper.writeValueAsString(todo);

        return given().contentType(ContentType.JSON)
                .body(todoJson)
                .when()
                .auth()
                .basic("admin", "admin")
                .post("http://localhost:8080/todos");
    }


    public static void createExistingTodoThroughApi(Todo todo) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String todoJson = objectMapper.writeValueAsString(todo);

        given().contentType(ContentType.JSON)
                .body(todoJson)
                .when()
                .auth()
                .basic("admin", "admin")
                .post("http://localhost:8080/todos")
                .then()
                .statusCode(400);
    }


    public static List<Todo> getTodosThroughApi() throws JsonProcessingException {
        return given()
                .contentType(ContentType.JSON)
                .when()
                .auth()
                .basic("admin", "admin")
                .get("http://localhost:8080/todos")
                .then()
                .statusCode(200)
                .extract()
                .body()
                .jsonPath()
                .getList(".", Todo.class);
    }

    public static List<Todo> getTodosWithOffsetThroughApi(int offset) throws JsonProcessingException {
        return given()
                .queryParam("offset", offset)
                .contentType(ContentType.JSON)
                .when()
                .auth()
                .basic("admin", "admin")
                .get("http://localhost:8080/todos")
                .then()
                .statusCode(200)
                .extract()
                .body()
                .jsonPath()
                .getList(".", Todo.class);
    }

    public static List<Todo> getTodosWithLimitThroughApi(int limit) throws JsonProcessingException {
        return given()
                .queryParam("limit", limit)
                .contentType(ContentType.JSON)
                .when()
                .auth()
                .basic("admin", "admin")
                .get("http://localhost:8080/todos")
                .then()
                .statusCode(200)
                .extract()
                .body()
                .jsonPath()
                .getList(".", Todo.class);
    }

    public static List<Todo> getTodosWithOffsetAndLimitThroughApi(int offset, int limit) throws JsonProcessingException {
        return given()
                .queryParam("offset", offset)
                .queryParam("limit", limit)
                .contentType(ContentType.JSON)
                .when()
                .auth()
                .basic("admin", "admin")
                .get("http://localhost:8080/todos")
                .then()
                .statusCode(200)
                .extract()
                .body()
                .jsonPath()
                .getList(".", Todo.class);
    }


    public static void deleteTodoThroughApi(int id) {
        given().header("Accept", "*/*")
                .when()
                .auth()
                .preemptive()
                .basic("admin", "admin")
                .delete("http://localhost:8080/todos/" + id)
                .then()
                .statusCode(204);
    }

    public static Response deleteTodoThroughApiNotChecked(int id) {
        return given().header("Accept", "*/*")
                .when()
                .auth()
                .preemptive()
                .basic("admin", "admin")
                .delete("http://localhost:8080/todos/" + id);
    }

    public static void changeTodoThroughApi(Todo todo) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String todoJson = objectMapper.writeValueAsString(todo);

        given().contentType(ContentType.JSON)
                .body(todoJson)
                .when()
                .auth()
                .basic("admin", "admin")
                .put("http://localhost:8080/todos/" + todo.getId())
                .then()
                .statusCode(200);
    }

    public static void changeNonExistingTodoThroughApi(Todo todo) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String todoJson = objectMapper.writeValueAsString(todo);

        given().contentType(ContentType.JSON)
                .body(todoJson)
                .when()
                .auth()
                .basic("admin", "admin")
                .put("http://localhost:8080/todos/" + todo.getId())
                .then()
                .statusCode(404);
    }

    public static void changeTodoDifferentIdThroughApi(Todo todo, int id) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        String todoJson = objectMapper.writeValueAsString(todo);

        given().contentType(ContentType.JSON)
                .body(todoJson)
                .when()
                .auth()
                .basic("admin", "admin")
                .put("http://localhost:8080/todos/" + id)
                .then()
                .statusCode(422);
    }

}
